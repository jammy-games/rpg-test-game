#!/usr/bin/env python3

import random
import math
from sys import argv

# Parse command line arguments
def main():
    if len(argv) > 1:
        c1Attack = int(argv[1])
        c1Speed = int(argv[2])
        c1Defense = int(argv[3])
        c1Health = int(argv[4])
        c2Attack = int(argv[5])
        c2Speed = int(argv[6])
        c2Defense = int(argv[7])
        c2Health = int(argv[8])
        c1 = Character()
        c2 = Character()
        c1.setStats(c1Attack, c1Speed, c1Defense, c1Health)
        c2.setStats(c2Attack, c2Speed, c2Defense, c2Health)
        c1.printStats()
        c2.printStats()
        result = combat(c1, c2)
        print(result[0])
        print(result[1])
    else:
        print("Running built in simulation suite...")
        simulationSuite()

class Character:
    attack = 1
    speed = 1
    defense = 1
    health = 9

    def setStats(self, attack, speed, defense, health):
        self.attack = attack
        self.speed = speed
        self.defense = defense
        self.health = health

    def dice(self, rolls, sides):
        #return 0
        total = 0
        for roll in range(rolls):
            total = total + int(random.random() * sides)
        return total

    def dice_2d4(self):
        #return 0
        return int(random.random() * 4) + int(random.random() * 4) + 2

    def getVariableAttack(self):
        return self.dice(1, 4) + self.attack

    def getVariableSpeed(self):
        return self.dice(1, 4) + self.speed

    def getVariableDefense(self):
        return self.dice(1, 4) + self.defense

    def printStats(self):
        print("attack:", self.attack,
            "speed:", self.speed,
            "defense:", self.defense)
        print("health:", self.health)

def hits(c1, c2):
    damage = c1.getVariableAttack() + c1.getVariableSpeed()
    damage = damage - c2.getVariableDefense() 
    if damage < 0:
        damage = 0
    #print("damage:", damage)
    c2.health = c2.health - damage

def attack(c1, c2):
    hits(c1, c2)

def simultaneous(c1, c2):
    attack(c1, c2)
    attack(c2, c1)

def first(c1, c2):
    attack(c1, c2)
    if c2.health <= 0:
        return
    attack(c2, c1)

def round(c1, c2):
    c1Speed = c1.getVariableSpeed()
    c2Speed = c2.getVariableSpeed()
    if c1Speed > c2Speed:
        first(c1, c2)
    elif c2Speed > c1Speed:
        first(c2, c1)
    else:
        simultaneous(c1, c2)

def combat(c1, c2):
    summary = ""
    roundCount = 0
    c1StartHealth = c1.health
    c2StartHealth = c2.health
    while c1.health > 0 and c2.health > 0:
        if (
                roundCount > 10 and
                c1.health == c1StartHealth and
                c2.health == c2StartHealth
            ):
            return ["stalemate", summary]
        round(c1, c2)
        roundCount += 1
        summary += "c1 health: " + str(c1.health) 
        summary += " | c2 health:" + str(c2.health) + "\n"
        #print (summary)
    if c1.health > 0:
        return ["c1 wins", summary]
    elif c2.health > 0:
        return ["c2 wins", summary]
    else:
        return ["draw", summary]

def simulationSuite():
    resultList = []
    resultList.append(simulateNCombats(1, 1, 1, 30, 1, 1, 1, 30, 1000))
    resultList.append(simulateNCombats(2, 1, 1, 40, 2, 1, 1, 40, 1000))
    resultList.append(simulateNCombats(2, 1, 1, 40, 1, 2, 1, 40, 1000))
    resultList.append(simulateNCombats(2, 1, 1, 40, 1, 1, 2, 40, 1000))
    resultList.append(simulateNCombats(1, 2, 1, 40, 1, 2, 1, 40, 1000))
    resultList.append(simulateNCombats(1, 2, 1, 40, 1, 1, 2, 40, 1000))
    resultList.append(simulateNCombats(1, 1, 2, 40, 1, 1, 2, 40, 1000))
    resultList.append(simulateNCombats(3, 1, 1, 50, 3, 1, 1, 50, 1000))
    resultList.append(simulateNCombats(1, 3, 1, 50, 1, 3, 1, 50, 1000))
    resultList.append(simulateNCombats(1, 1, 3, 50, 1, 1, 3, 50, 1000))
    resultList.append(simulateNCombats(3, 1, 1, 50, 1, 3, 1, 50, 1000))
    resultList.append(simulateNCombats(3, 1, 1, 50, 1, 1, 3, 50, 1000))
    resultList.append(simulateNCombats(1, 3, 1, 50, 1, 1, 3, 50, 1000))
    resultList.append(simulateNCombats(2, 2, 1, 50, 3, 1, 1, 50, 1000))
    resultList.append(simulateNCombats(2, 1, 2, 50, 3, 1, 1, 50, 1000))
    resultList.append(simulateNCombats(1, 2, 2, 50, 3, 1, 1, 50, 1000))
    resultList.append(simulateNCombats(2, 2, 1, 50, 1, 3, 1, 50, 1000))
    resultList.append(simulateNCombats(2, 1, 2, 50, 1, 3, 1, 50, 1000))
    resultList.append(simulateNCombats(1, 2, 2, 50, 1, 3, 1, 50, 1000))
    resultList.append(simulateNCombats(2, 2, 1, 50, 1, 1, 3, 50, 1000))
    resultList.append(simulateNCombats(2, 1, 2, 50, 1, 1, 3, 50, 1000))
    resultList.append(simulateNCombats(1, 2, 2, 50, 1, 1, 3, 50, 1000))

    for results in resultList:
        print (
                "stats:", results[0],
                "| c1 wins:", results[1],
                "| c2 wins:", results[2],
                "| draws", results[3],
                "| stalemates", results[4],
                "| c1 win health av.:", results[5], 
                "| c2 win health av.:", results[6]) 

def simulateNCombats(
        c1Attack, c1Speed, c1Defense, c1Health,
        c2Attack, c2Speed, c2Defense, c2Health,
        n):
    c1 = Character()
    c2 = Character()
    c1.setStats(c1Attack, c1Speed, c1Defense, c1Health)
    c2.setStats(c2Attack, c2Speed, c2Defense, c2Health)
    c1Wins = 0
    c2Wins = 0
    draws = 0
    stalemates = 0
    totalC1WinHealth = 0
    totalC2WinHealth = 0
    for each in range(n):
        combatResult = combat(c1, c2)
        result = combatResult[0]
        if result == "c1 wins":
            c1Wins += 1
            totalC1WinHealth += c1.health
        elif result == "c2 wins":
            c2Wins += 1
            totalC2WinHealth += c2.health
        elif result == "draw":
            draws += 1
        elif result == "stalemate":
            stalemates += 1
        c1.setStats(c1Attack, c1Speed, c1Defense, c1Health)
        c2.setStats(c2Attack, c2Speed, c2Defense, c2Health)
    averageC1WinHealth = 0 if c1Wins == 0 else totalC1WinHealth / c1Wins
    averageC2WinHealth = 0 if c2Wins == 0 else totalC2WinHealth / c2Wins
    return [
        str(c1Attack) + ", " +
        str(c1Speed) + ", " +
        str(c1Defense) + ", " +
        str(c1Health) + ", " +
        str(c2Attack) + ", " +
        str(c2Speed) + ", " +
        str(c2Defense) + ", " +
        str(c2Health),
        c1Wins,
        c2Wins,
        draws,
        stalemates,
        averageC1WinHealth,
        averageC2WinHealth
    ]

main()

