package input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayDeque;

import mainApp.InputCommand;

/**
 * Handles the input for the game.
 * 
 * @author Jammy Games
 * 
 */
public class Controller implements KeyListener {

    private int keyCode;
    // private ArrayDeque<InputCommand> inputCommands;
    private InputCommand inputCommand;
    private InputCommand previousInputCommand;
    private boolean keyDown;

    /**
     * Constructs a new Input object.
     * 
     * @param game
     *        The Game object to handle input for.
     * @param mapView
     *        The View object to update on input event.
     * @param mapPanel
     *        The MapPanel to refresh once view is updated.
     */
    public Controller () {

        // previousInputCommand = InputCommand.NOTHING;
        inputCommand = InputCommand.NOTHING;
        // inputCommands = new ArrayDeque<InputCommand>();
    }

    public InputCommand getInputCommand() {

        // System.out
        // .println("Controller.getInputCommand(): inputCommands.size(): "
        // + inputCommands.size());
        // if (inputCommands.size() > 0) return inputCommands.remove();
        // else return InputCommand.NOTHING;
        return inputCommand;
    }

    @Override
    /**
     * Implements the keyPressed method. Invoked when a key is pressed.
     */
    public void keyPressed(KeyEvent keyEvent) {

        if (!keyDown) {
            keyCode = keyEvent.getKeyCode();
            inputCommand = keyCodeToInputCommand(keyCode);
        }
        else {
            inputCommand = InputCommand.NOTHING;
        }
        keyDown = true;
    }

    @Override
    /**
     * Implements the keyPressed method. Invoked when a key is released.
     */
    public void keyReleased(KeyEvent e) {

        keyDown = false;
        inputCommand = InputCommand.NOTHING;
    }

    private InputCommand keyCodeToInputCommand(int keyCode) {

        switch (keyCode) {
            case 10:
                return InputCommand.ENTER;
            case 32:
                return InputCommand.PROGRESS;
            case 37:
                return InputCommand.LEFT;
            case 38:
                return InputCommand.UP;
            case 39:
                return InputCommand.RIGHT;
            case 40:
                return InputCommand.DOWN;
            case 65:
                return InputCommand.ATTACK;
            case 70:
                return InputCommand.FLEE;
            case 73:
                return InputCommand.ITEMS;
            case 88:
                return InputCommand.EXIT;
            default:
                return InputCommand.NOTHING;
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

        // Do nothing
    }
}
