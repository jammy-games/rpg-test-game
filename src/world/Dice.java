package world;

public final class Dice {

    public static int roll(int sides) {

        return (int)(Math.random() * sides) + 1;
    }

    public static int roll(int quantity, int sides) {

        int sum = 0;
        for (; quantity > 0; quantity--)
            sum += (int)(Math.random() * sides) + 1;
        return sum;
    }
}
