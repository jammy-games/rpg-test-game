package world;

public class Tile {

	private Terrain terrain;
	private boolean occupied;
	private boolean passable;
	private GameObject occupier;

	public Tile() {
		terrain = Terrain.GRASS;
		passable = true;
		occupied = false;
	}

	public Tile(Terrain terrain) {
		this.terrain = terrain;
		passable = passable(terrain);
		occupied = false;
	}

	public boolean passable() {
		return passable;
	}

	public Terrain getTerrain() {
		return terrain;
	}

	public boolean isOccupied() {
		return occupied;
	}

	public void setNotOccupied() {
		occupied = false;
		occupier = null;
	}

	private boolean passable(Terrain terrain) {
		switch (terrain) {
		case FOREST:
			return false;
		case GRASS:
			return true;
		case MOUNTAIN:
			return false;
		case WATER:
			return false;
		case DOOR:
			return true;
		case EMPTY:
		    return false;
		default:
			return false;
		}
	}

	public GameObject getOccupier() {
		return occupier;
	}

	public void setOccupier(GameObject occupier) {
		this.occupier = occupier;
		if (occupier != null)
			occupied = true;
		else
			occupied = false;
	}

}
