/**
 * 
 */
package world;

import java.awt.Color;
import java.awt.Image;

/**
 * @author fin
 * 
 */
public class HealthPotion extends ConsumableItem {

    int healingValue;

    /**
     * @param location
     * @param color
     * @param name
     */
    public HealthPotion (Location location, Color color, String name) {

        super(location, color, name);
        healingValue = 21;
    }

    /**
     * @param location
     * @param color
     * @param name
     */
    public HealthPotion (Location location) {

        super(location, new Color(0, 255, 0), "Health Potion");
        healingValue = 21;
    }

    @Override
    public void consume(Character target) {

        if (!consumed) {
            target.heal((Dice.roll((int)(target.getMaxHealth() / 10), 6)));
            consumed = true;
        }
    }

    /**
     * @return consumed
     */
    public boolean isConsumed() {

        return consumed;
    }

    @Override
    public Image getSprite() {

        // TODO Auto-generated method stub
        System.out.println("No sprite for HealthPotion yet");
        return null;
    }
}
