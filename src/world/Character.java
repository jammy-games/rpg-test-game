package world;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Character implements GameObject {

    private Location location;
    private Color color;
    private int health;
    private int maxHealth;
    private boolean alive;
    private String name;
    private int attack;
    private int speed;
    private int defense;
    private Bag bag;
    private boolean moved;
    private int kills;
    private BufferedImage sprite;

    public Character (
        String name,
        Location location,
        Color color,
        int attack,
        int speed,
        int defense,
        int health,
        String spriteFilePath) {
        
        this(name, location, color, spriteFilePath);
        this.health = health;
        maxHealth = health;
        this.attack = attack;
        this.speed = attack;
        this.defense = attack;
        kills = 0;
    }

    public Character (
            String name,
            Location location,
            Color color,
            int level,
            String spriteFilePath) {

        this(name, location, color, spriteFilePath);
        generateStatsAtLevel(level);
    }

    private Character (
            String name,
            Location location,
            Color color,
            String spriteFilePath) {

        moved = false;
        bag = new Bag(location, color, "Useless Bag", 0);
        alive = true;
        this.name = name;
        this.location = location;
        this.color = color;
        try {
            sprite = ImageIO.read(getClass().getResource(spriteFilePath));
            System.out.println("Character.Character(): created character sprite for " + name);
        }
        catch (IOException e) {
            System.err.println("Cannot load character sprite from "
                + spriteFilePath);
            e.printStackTrace();
        }
    }

    private void generateStatsAtLevel(int level) {
        health = (level + 2) * 10;
        attack = 1;
        speed = 1;
        defense = 1;
        level--;
        while (level > 0) {
            int d3 = Dice.roll(3);
            switch (d3) {
                case 1:
                    attack++;
                    break;
                case 2:
                    speed++;
                    break;
                case 3:
                    defense++;
                    break;
                default:
                    break;
            }
            level--;
        }
        maxHealth = health;
    }

    public void consumeItem(ConsumableItem item) {

        item.consume(this);
    }

    public void equipItem(EquipableItem item) {

        item.equip(this);
    }

    public Location getLocation() {

        return location;
    }

    public Color getColor() {

        return color;
    }

    public int getHealth() {

        return health;
    }

    public void takeDamage(int damage) {

        int potentialHealth = health - damage;
        if (potentialHealth < 0) {
            health = 0;
            alive = false;
        }
        else {
            health = potentialHealth;
        }
    }

    /**
     * Teleport the Character to the location specified by x and y.
     * 
     * @param x
     *        The new x axis coordinate.
     * @param y
     *        The new y axis coordinate.
     */
    public void teleport(int x, int y) {

        location.setX(x);
        location.setY(y);
    }

    @Override
    public void setLocation(Location location) {

        this.location = location;
    }

    public void moveLeft() {

        location.moveLeft();
        moved = true;
    }

    public void moveUp() {

        location.moveUp();
        moved = true;
    }

    public void moveRight() {

        location.moveRight();
        moved = true;
    }

    public void moveDown() {

        location.moveDown();
        moved = true;
    }

    @Override
    public String getName() {

        return name;
    }

    public int getAttack() {

        return attack;
    }

    public int getSpeed() {

        return speed;
    }

    public int getDefense() {

        return defense;
    }

    public int getMaxHealth() {

        return maxHealth;
    }

    /**
     * @return the bag
     */
    public Bag getBag() {

        return bag;
    }

    /**
     * @param bag
     *        the bag to set
     */
    public void setBag(Bag bag) {

        this.bag = bag;
    }

    /**
     * @return the alive
     */
    public boolean isAlive() {

        return alive;
    }

    public void heal(int healingValue) {

        int potentialHealth = health + healingValue;
        if (potentialHealth > maxHealth) {
            health = maxHealth;
        }
        else {
            health = potentialHealth;
        }
    }

    /**
     * 
     * @return if the player character moved location after last input.
     */
    public boolean moved() {

        return moved;
    }

    public void setMoved(boolean moved) {

        this.moved = moved;
    }

    /**
     * @return the kills
     */
    public int getKills() {

        return kills;
    }

    /**
     * @param kills
     *        the kills to add
     */
    public void killed(int kills) {

        this.kills += kills;
    }

    @Override
    public Image getSprite() {

        return sprite;
    }
}
