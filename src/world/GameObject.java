package world;

import java.awt.Color;
import java.awt.Image;

public interface GameObject {

    public Location getLocation();

    public void setLocation(Location location);

    public Color getColor();

    public String getName();

    public Image getSprite();
}
