package world;

import java.awt.Color;

public class Door extends Tile {

    private GameMap toMap;
    private int toMapX;
    private int toMapY;

    public Door(GameMap toMap, int toMapX, int toMapY, Color color) {
	super(Terrain.DOOR);
	this.toMap = toMap;
	this.toMapX = toMapX;
	this.toMapY = toMapY;
    }

    public GameMap getToMap() {
	return toMap;
    }

    public int getToMapX() {
	return toMapX;
    }

    public int getToMapY() {
	return toMapY;
    }

}
