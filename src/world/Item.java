/**
 * 
 */
package world;

import java.awt.Color;

/**
 * @author fin
 * 
 */
public abstract class Item implements GameObject {

    private Location location;
    private Color color;
    private String name;

    /**
     * Create new item with it's location, colour, and name.
     */
    public Item (Location location, Color color, String name) {
        this.location = location;
        this.color = color;
        this.name = name;
    }

    /**
     * returns Item's location
     */
    @Override
    public Location getLocation () {
        return location;
    }

    /**
     * sets the Item's location
     */
    @Override
    public void setLocation (Location location) {
        this.location = location;

    }

    /**
     * @return color The colour of the Item
     */
    @Override
    public Color getColor () {
        return color;
    }

    /**
     * Returns name of Item
     */
    @Override
    public String getName () {
        return name;
    }

}
