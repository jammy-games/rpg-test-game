package world;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class GameMap {

    private int width;
    private int height;
    private Tile[][] tiles;
    private int battleFrequency;
    private int enemyLevel;
    private Tile empty;

    public GameMap (int width, int height, int battleFrequency) {

        this.width = width;
        this.height = height;
        this.battleFrequency = battleFrequency;
        enemyLevel = 1;
        tiles = new Tile[width][height];
        // empty = new Tile(Terrain.EMPTY);
        populateMap();
    }

    public GameMap (String mapFile, int enemyLevel, int battleFrequency) {

        ArrayList<char[]> mapData = new ArrayList<char[]>();
        int maxLineLength = 0;
        int lineCount = 0;
        try {
        	
            BufferedReader br =
                new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/resources/map-example.txt")));
            String line = null;
            while ((line = br.readLine()) != null) {
                lineCount++;
                char[] lineArray = line.toCharArray();
                mapData.add(lineArray);
                int lineLength = lineArray.length;
                if (lineLength > maxLineLength) maxLineLength = lineLength;
                for (char c : lineArray) {
                    System.out.print(c);
                }
                System.out.println();
            }
            br.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println("Could not load map-example.txt");
        }
        this.battleFrequency = battleFrequency;
        this.width = maxLineLength;
        this.height = lineCount;
        System.out.println("maxLineLength/width: " + maxLineLength);
        System.out.println("lineCount/height: " + lineCount);
        this.enemyLevel = enemyLevel;
        this.tiles = new Tile[width][height];
        for (int y = 0; y < height; y++) {
            char[] line = mapData.get(y);
            for (int x = 0; x < width; x++) {
                System.out.print(line[x]);
                tiles[x][y] = tileFromChar(line[x]);
            }
            System.out.println();
        }
    }

    private Tile tileFromChar(char tile) {

        switch (tile) {
            case 'G':
                return new Tile(Terrain.GRASS);
            case 'M':
                return new Tile(Terrain.MOUNTAIN);
            case 'W':
                return new Tile(Terrain.WATER);
            case 'F':
                return new Tile(Terrain.FOREST);
            case 'D':
                return new Tile(Terrain.DOOR);
            case 'E':
                return new Tile(Terrain.EMPTY);
            default:
                return new Tile(Terrain.EMPTY);
        }
    }

    public int getWidth() {

        return width;
    }

    public int getHeight() {

        return height;
    }

    public int getBattleFrequency() {

        return battleFrequency;
    }

    public int getEnemyLevel() {

        return enemyLevel;
    }

    public void setEnemyLevel(int enemyLevel) {

        this.enemyLevel = enemyLevel;
    }

    private void populateMap() {

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                if (x <= 1 || x >= width - 2 || y <= 1 || y >= height - 2) {
                    tiles[x][y] = new Tile(Terrain.MOUNTAIN);
                }
                else if (x <= 3 || x >= width - 4 || y <= 3 || y >= height - 4) {
                    tiles[x][y] = new Tile(Terrain.FOREST);
                }
                else if (y % 3 == 1 && x % 3 == 1) {
                    tiles[x][y] = new Tile(Terrain.FOREST);
                }
                else {
                    tiles[x][y] = new Tile(Terrain.GRASS);
                }
            }
        }
    }

    //
    // public void populateMap(String file) {
    // for (int x = 0; x < width; x++) {
    // for (int y = 0; y < height; y++) {
    // if (x <= 1 || x >= width - 2 || y <= 1 || y >= height - 2) {
    // tiles[x][y] = new Tile(Terrain.MOUNTAIN);
    // } else if (x <= 3 || x >= width - 4 || y <= 3 || y >= height - 4) {
    // tiles[x][y] = new Tile(Terrain.FOREST);
    // } else if (y % 3 == 1 && x % 3 == 1) {
    // tiles[x][y] = new Tile(Terrain.FOREST);
    // } else {
    // tiles[x][y] = new Tile(Terrain.GRASS);
    // }
    // }
    // }
    // }
    public void connectMap(
        GameMap gameMap,
        int fromTileX,
        int fromTileY,
        int toTileX,
        int toTileY) {

        tiles[fromTileX][fromTileY] =
            new Door(gameMap, toTileX, toTileY, new Color(0, 0, 0));
    }

    public String toString() {

        String mapString = "";
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < width; y++) {
                mapString += (tiles[x][y].getTerrain() + ", ");
            }
            mapString += '\n';
        }
        return mapString;
    }

    public Tile getTile(int x, int y) {

        if (x < width && y < height && x >= 0 && y >= 0) return tiles[x][y];
        else return empty;
    }

    public Tile getEntrance() {

        return tiles[width / 2][height / 2];
    }
}
