/**
 * 
 */
package world;

import java.awt.Color;

/**
 * @author fin
 *
 */
public abstract class ConsumableItem extends Item {

    boolean consumed;

    /**
     * @param location
     * @param color
     * @param name
     */
    public ConsumableItem (Location location, Color color, String name) {
        super(location, color, name);
        consumed = false;
    }

    public abstract void consume (Character target);

}
