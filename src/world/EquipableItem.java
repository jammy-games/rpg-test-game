/**
 * 
 */
package world;

import java.awt.Color;

/**
 * @author fin
 * 
 */
public abstract class EquipableItem extends Item {

    /**
     * @param location
     * @param color
     * @param name
     */
    public EquipableItem (Location location, Color color, String name) {
        super(location, color, name);
        // TODO Auto-generated constructor stub
    }

    /**
     * 
     * @param target The character that is equipping the item.
     */
    public abstract void equip (Character target);

}
