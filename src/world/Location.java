package world;

public class Location {

    private GameObject gameObject;
    private GameMap map;
    private int x;
    private int y;

    public Location(GameObject gameObject, GameMap map, int x, int y) {
	this.map = map;
	this.x = x;
	this.y = y;
	this.gameObject = gameObject;
	this.map.getTile(x, y).setOccupier(gameObject);
    }

    public int getX() {
	return x;
    }

    public int getY() {
	return y;
    }

    public void setX(int x) {
	map.getTile(x, y).setOccupier(null);
	this.x = x;
	updateMapTileOccupier();
    }

    public void setY(int y) {
	map.getTile(x, y).setOccupier(null);
	this.y = y;
	updateMapTileOccupier();
    }
    public void moveUp() {
	map.getTile(x, y).setOccupier(null);
	y -= 1;
	updateMapTileOccupier();
    }

    public void moveDown() {
	map.getTile(x, y).setOccupier(null);
	y += 1;
	updateMapTileOccupier();
    }

    public void moveLeft() {
	map.getTile(x, y).setOccupier(null);
	x -= 1;
	updateMapTileOccupier();
    }

    public void moveRight() {
	map.getTile(x, y).setOccupier(null);
	x += 1;
	updateMapTileOccupier();
    }

    public GameMap getGameMap() {
	return map;
    }

    public Terrain getTerrain() {
	return map.getTile(x, y).getTerrain();
    }

    public Tile getTile() {
	return map.getTile(x, y);
    }

    public GameObject getGameObject() {
	return gameObject;
    }

    public void setGameObject(GameObject gameObject) {
	this.gameObject = gameObject;
	updateMapTileOccupier();
    }

    private void updateMapTileOccupier() {
	map.getTile(x, y).setOccupier(gameObject);

    }

}
