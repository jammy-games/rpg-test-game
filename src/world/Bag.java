/**
 * 
 */
package world;

import java.awt.Color;
import java.awt.Image;
import java.util.ArrayList;

/**
 * @author fin
 * 
 */
public class Bag extends EquipableItem {

    private ArrayList<Item> items;
    private int capacity;

    public Bag(Location location, Color color, String name, int capacity) {
	super(location, color, name);
	this.items = new ArrayList<Item>();
	this.capacity = capacity;
    }

    @Override
    public void equip(Character target) {
	// TODO Auto-generated method stub

    }

    public void addItem(Item item) throws Exception {
	if (items.size() < capacity)
	    items.add(item);
	else
	    throw new Exception("Bag.addItem(" + item
		    + "): not enough space in Bag for item.");
    }

    public void removeItem(Item item) throws Exception {
	if (items.contains(item)) {
	    items.remove(item);
	} else {
	    throw new Exception("Item " + item.getName() + " not in bag.");
	}

    }

    public ArrayList<Item> getItems() {
	return items;
    }

    public String listItems() {
	String itemList = "";
	for (Item item : items) {
	    itemList += ", " + item.getName();
	}
	if (itemList.length() > 2) {
	    return itemList.substring(2);
	}
	return "empty";
    }

    @Override
    public Image getSprite() {

        // TODO Auto-generated method stub
        System.out.println("No sprite for Bag");
        return null;
    }

}
