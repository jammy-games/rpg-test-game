/**
 * 
 */
package world;

import mainApp.InputCommand;

/**
 * @author fin
 * 
 */
public class Overworld {

    private int viewSize;
    private PlayerCharacter playerCharacter;
    private boolean inProgress;

    /**
     * 
     */
    public Overworld (int viewSize, PlayerCharacter playerCharacter) {

        // this.viewSize = viewSize;
        this.playerCharacter = playerCharacter;
        inProgress = false;
    }

    public void update(InputCommand inputCommand) {

        playerCharacterMovement(inputCommand);
        if (playerCharacter.moved()) playerCharacter
            .decrementStepsForNextBattle();
        // If the Player moves to a door
        if (playerCharacter
            .getLocation()
            .getGameMap()
            .getTile(
                playerCharacter.getLocation().getX(),
                playerCharacter.getLocation().getY()) instanceof Door
            && playerCharacter.moved()) {
            Door door =
                ((Door)playerCharacter
                    .getLocation()
                    .getGameMap()
                    .getTile(
                        playerCharacter.getLocation().getX(),
                        playerCharacter.getLocation().getY()));
            playerCharacter.setLocation(new Location(playerCharacter, door
                .getToMap(), door.getToMapX(), door.getToMapY()));
            playerCharacter.resetStepsForNextBattle();
        }
//        System.out.println("Overworld.update(): "
//            + playerCharacter.getLocation());
        System.out.println("Overworld.update(): stepsForNextBattle "
            + playerCharacter.getStepsForNextBattle());
    }

    private void playerCharacterMovement(InputCommand inputCommand) {

        switch (inputCommand) {
            case LEFT:
                if (!playerAtLeftMapEdge() && !playerBlockedLeft()) playerCharacter
                    .moveLeft();
                else {
                    playerCharacter.setMoved(false);
                    System.out.println("Hit left edge of map.");
                }
                break;
            case UP:
                if (!playerAtTopMapEdge() && !playerBlockedUp()) playerCharacter
                    .moveUp();
                else {
                    playerCharacter.setMoved(false);
                    System.out.println("Hit top of map.");
                }
                break;
            case RIGHT:
                if (!playerAtRightMapEdge() && !playerBlockedRight()) playerCharacter
                    .moveRight();
                else {
                    playerCharacter.setMoved(false);
                    System.out.println("Hit right edge of map.");
                }
                break;
            case DOWN:
                if (!playerAtBottomMapEdge() && !playerBlockedDown()) playerCharacter
                    .moveDown();
                else {
                    playerCharacter.setMoved(false);
                    System.out.println("Hit bottom of map.");
                }
                break;
            default:
                playerCharacter.setMoved(false);
                break;
        }
        System.out.println("Player location: "
            + playerCharacter.getLocation().getX()
            + ", "
            + playerCharacter.getLocation().getY());
    }

    private boolean playerBlockedUp() {

        Location pcLocation = playerCharacter.getLocation();
        boolean passableLeftTile =
            pcLocation
                .getGameMap()
                .getTile(pcLocation.getX(), pcLocation.getY() - 1)
                .passable();
        return !passableLeftTile;
    }

    private boolean playerBlockedRight() {

        Location pcLocation = playerCharacter.getLocation();
        boolean passableLeftTile =
            pcLocation
                .getGameMap()
                .getTile(pcLocation.getX() + 1, pcLocation.getY())
                .passable();
        return !passableLeftTile;
    }

    private boolean playerBlockedDown() {

        Location pcLocation = playerCharacter.getLocation();
        boolean passableLeftTile =
            pcLocation
                .getGameMap()
                .getTile(pcLocation.getX(), pcLocation.getY() + 1)
                .passable();
        return !passableLeftTile;
    }

    private boolean playerBlockedLeft() {

        Location pcLocation = playerCharacter.getLocation();
        boolean passableLeftTile =
            pcLocation
                .getGameMap()
                .getTile(pcLocation.getX() - 1, pcLocation.getY())
                .passable();
        return !passableLeftTile;
    }

    private boolean playerAtLeftMapEdge() {

        int playerX = playerCharacter.getLocation().getX();
        if (playerX - ((viewSize - 1) / 2) > 0) return false;
        else return true;
    }

    private boolean playerAtRightMapEdge() {

        int mapWidth = playerCharacter.getLocation().getGameMap().getWidth();
        int playerX = playerCharacter.getLocation().getX();
        if (playerX + ((viewSize - 1) / 2) < (mapWidth - 1)) return false;
        else return true;
    }

    private boolean playerAtTopMapEdge() {

        int playerY = playerCharacter.getLocation().getY();
        if (playerY - ((viewSize - 1) / 2) > 0) return false;
        else return true;
    }

    private boolean playerAtBottomMapEdge() {

        int mapHeight = playerCharacter.getLocation().getGameMap().getHeight();
        int playerY = playerCharacter.getLocation().getY();
        if (playerY + ((viewSize - 1) / 2) < (mapHeight - 1)) return false;
        else return true;
    }

    public boolean inProgress() {

        return inProgress;
    }

    public void setInProgress(boolean inProgress) {

        this.inProgress = inProgress;
    }
}
