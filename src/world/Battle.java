/**
 * 
 */
package world;

import mainApp.InputCommand;

/**
 * @author fin
 * 
 */
public class Battle {

    private PlayerCharacter playerCharacter;
    private Character enemy;
    private int pcSpeed;
    private int enemySpeed;
    private String message;
    private String previousMessage;
    private boolean inProgress;
    private boolean battleOver;

    public Battle (PlayerCharacter playerCharacter, Character enemy) {

        this.message = "";
        this.playerCharacter = playerCharacter;
        this.enemy = enemy;
        pcSpeed = playerCharacter.getSpeed();
        enemySpeed = enemy.getSpeed();
        updateMessage(enemy.getName() + " appeared!");
        inProgress = true;
        battleOver = false;
    }

    public void attack() {

        updateMessage("You attack!");
        // Determine who hits first by rolling for initiative
        int pcInitiative = pcSpeed + Dice.roll(6);
        int enemyInitiative = enemySpeed + Dice.roll(6);
        // Each attacks in order of initiative
        if (pcInitiative >= enemyInitiative) {
            playerAttacks();
            if (enemy.isAlive()) enemyAttacks();
        }
        else {
            enemyAttacks();
            if (playerCharacter.isAlive()) playerAttacks();
        }
    }

    public boolean flee() {

        if (playerCharacter.getSpeed() + Dice.roll(6) > enemy.getSpeed()
            + Dice.roll(6)) {
            updateMessage("You flee!");
            return true;
        }
        else {
            updateMessage("The enemy was faster!");
            return false;
        }
    }

    public String getMessage() {

        return message;
    }

    public String getPreviousMessage() {

        return previousMessage;
    }

    private void playerAttacks() {

        int potentialDamage =
            playerCharacter.getAttack()
                + Dice.roll(4)
                + playerCharacter.getSpeed()
                + Dice.roll(4);
        int damage = potentialDamage - enemy.getDefense() + Dice.roll(4);
        enemy.takeDamage(damage);
        updateMessage(enemy.getName() + " took " + damage + " damage.");
        if (enemy.getHealth() < 1) {
            updateMessage("You win!");
            playerCharacter.killed(1);
        }
    }

    private void enemyAttacks() {

        int potentialDamage =
            enemy.getAttack() + Dice.roll(4) + enemy.getSpeed() + Dice.roll(4);
        int damage =
            potentialDamage - playerCharacter.getDefense() + Dice.roll(4);
        playerCharacter.takeDamage(damage);
        updateMessage("You took " + damage + " damage.");
        if (playerCharacter.getHealth() < 1) {
            updateMessage("You died!");
        }
    }

    public void tick(InputCommand inputCommand) {

        System.out.println("Game.battle(): Battle Mode");
        if (battleOver) {
            // show final message
            inProgress = false;
        }
        else {
            switch (inputCommand) {
                case ATTACK:
                    attack();
                    if (playerCharacter.getHealth() < 1
                        || enemy.getHealth() < 1) {
                        endBattle();
                    }
                    break;
                case FLEE:
                    if (flee()) {
                        endBattle();
                    }
                    else {
                        enemyAttacks();
                    }
                    break;
                default:
                    break;
            }
        }
    }

    private void endBattle() {

        battleOver = true;
        playerCharacter.resetStepsForNextBattle();
    }

    public boolean inProgress() {

        return inProgress;
    }

    public void setInProgress(boolean inProgress) {

        this.inProgress = inProgress;
    }

    private void updateMessage(String newMessage) {

        previousMessage = message;
        message = newMessage;
    }
}
