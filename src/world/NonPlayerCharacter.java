package world;

import java.awt.Color;

public class NonPlayerCharacter extends Character {

    public NonPlayerCharacter (
        String name,
        Location location,
        Color color,
        int health,
        String spriteFilePath) {

        super(name, location, color, 1, 1, 1, health, spriteFilePath);
    }
}
