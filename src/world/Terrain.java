package world;

public enum Terrain {
    GRASS, STONE, MOUNTAIN, WATER, FOREST, DOOR, EMPTY;
}
