package world;

import java.awt.Color;

public class PlayerCharacter extends Character {

    private int stepsForNextBattle;
    private final int NO_BATTLES = -1;

    public PlayerCharacter (
        String name,
        Location location,
        Color color,
        int level, String spriteFilePath) {

        super(name, location, color, level, spriteFilePath);
    }

    public int getStepsForNextBattle() {

        return stepsForNextBattle;
    }

    public void setStepsForNextBattle(int stepsForNextBattle) {

        this.stepsForNextBattle = stepsForNextBattle;
    }

    public void decrementStepsForNextBattle() {

        if (stepsForNextBattle > 0) stepsForNextBattle--;
    }

    public void resetStepsForNextBattle() {

        stepsForNextBattle =
            stepsUntilNextBattle(this
                .getLocation()
                .getGameMap()
                .getBattleFrequency());
    }

    private int stepsUntilNextBattle(int battleFrequency) {

        int steps = NO_BATTLES;
        for (int i = 0; i < battleFrequency; i++) {
            steps = steps + Dice.roll(6);
        }
        return steps;
    }
}
