/**
 * 
 */
package world;

import java.util.ArrayList;

import mainApp.InputCommand;

/**
 * @author fin
 * 
 */
public class Menu {

    private Character playerCharacter;
    private ArrayList<Item> items;
    private int selectedItem;
    private String message;
    private boolean inProgress;
    
    public void tick(InputCommand inputCommand) {

	switch (inputCommand) {
	case ENTER:
	    try {
		useItem(getSlectedItem());
		System.out.println("Game.menuTick(): used item");
		System.out.println(getMessage());
	    } catch (Exception e) {
		System.out.println(e);
	    }
	case ATTACK:
	    break;
	case DOWN:
	    selectNextItem();
	    break;
	case FLEE:
	    break;
	case ITEMS:
	    break;
	case LEFT:
	    break;
	case NOTHING:
	    break;
	case RIGHT:
	    break;
	case UP:
	    selectPreviousItem();
	    break;
	case EXIT:
	    setInProgress(false);
	    break;
	default:
	    break;
	}
    }

    /**
     * @param bag
     * 
     */
    public Menu(Character playerCharacter) {
	this.playerCharacter = playerCharacter;
	this.items = getItems();
	this.selectedItem = 0;
	message = "";
    }

    public ArrayList<Item> getItems() {
	return playerCharacter.getBag().getItems();
    }

    public Item getSlectedItem() {
	if (items.size() > 0)
	    return items.get(selectedItem);
	else
	    return null;
    }

    public String getMessage() {
	return message;
    }

    public void selectNextItem() {
	if (items.size() > 0) {
	    if (selectedItem == items.size() - 1)
		selectedItem = 0;
	    else
		selectedItem++;
	    System.out.println("selectedItem: " + selectedItem);
	}
    }

    public void selectPreviousItem() {
	if (items.size() > 0) {
	    if (selectedItem == 0)
		selectedItem = items.size() - 1;
	    else
		selectedItem--;
	    System.out.println("selectedItem: " + selectedItem);
	}
    }

    public void useItem(Item item) {
	if (item != null) {
	    if (item instanceof ConsumableItem) {
		System.out.println("Menu.useItem(): is consumable.");
		message = playerCharacter.getName() + " used " + item.getName();
		playerCharacter.consumeItem((ConsumableItem) item);
		try {
		    playerCharacter.getBag().removeItem(item);
		} catch (Exception e) {
		    e.printStackTrace();
		}
	    } else if (item instanceof EquipableItem) {
		playerCharacter.equipItem((EquipableItem) item);
		message = playerCharacter.getName() + " equipped "
			+ item.getName();
	    } else {
		message = "Item " + item.getName() + " cannot be used.";
	    }
	} else
	    message = "No items to use.";
	if (selectedItem >= items.size())
	    selectedItem--;
    }

    public boolean inProgress() {
	return inProgress;
    }

    public void setInProgress(boolean inProgress) {
	this.inProgress = inProgress;
	
    }

}
