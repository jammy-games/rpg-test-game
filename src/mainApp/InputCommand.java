package mainApp;

public enum InputCommand {
    LEFT, UP, RIGHT, DOWN, ATTACK, FLEE, ITEMS, EXIT, ENTER, NOTHING, PROGRESS;
}
