package mainApp;

import java.awt.Color;

import world.Bag;
import world.Battle;
import world.Character;
import world.GameMap;
import world.HealthPotion;
import world.Location;
import world.Menu;
import world.Overworld;
import world.PlayerCharacter;

/**
 * Model of a game. Represents and maintains the state of a game.
 * 
 * @author fin
 * 
 */
public class Game {

    private int viewSize;
    private Battle battle;
    private PlayerCharacter playerCharacter;
    private Character enemy;
    private GameMap town;
    private GameMap overworldMap;
    private GameMap testFromFile;
    private Overworld overworldState;
    private Menu menu;
    private GameMode gameMode;
    private GameMode previousGameMode;
    private int playerCharacterLevel;
    private InputCommand previousInputCommand;

    /**
     * Game constructor. Creates a new model of the game world.
     */
    public Game () {

        playerCharacterLevel = 5;
        testFromFile = new GameMap("map-example.txt", 2, 1);
        overworldMap = new GameMap(52, 52, 5);
        overworldMap.setEnemyLevel(3);
        town = new GameMap(17, 17, 0);
        town.connectMap(overworldMap, 7, 8, 6, 6);
        overworldMap.connectMap(town, 6, 6, 7, 8);
        overworldMap.connectMap(testFromFile, 12, 12, 34, 19);
        testFromFile.connectMap(overworldMap, 34, 19, 12, 12);
        gameMode = GameMode.OVERWORLD;
        previousGameMode = GameMode.OVERWORLD;
        previousInputCommand = InputCommand.NOTHING;
        playerCharacter =
            new PlayerCharacter(
                "Hero",
                null,
                new Color(255, 127, 0),
                playerCharacterLevel,
                "/resources/hero.png");
        playerCharacter.setLocation(new Location(playerCharacter, town, town
            .getWidth() / 2, town.getHeight() / 2));
        playerCharacter.setBag(new Bag(
            playerCharacter.getLocation(),
            new Color(127, 127, 127),
            "Brown Bag",
            10));
        for (int i = 8; i > 0; i--) {
            try {
                playerCharacter.getBag().addItem(
                    new HealthPotion(playerCharacter.getBag().getLocation()));
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        enemy =
            new Character("Default", playerCharacter.getLocation(), new Color(
                255,
                127,
                0), 1, "/resources/enemy.png");
        battle = new Battle(playerCharacter, enemy);
        battle.setInProgress(false);
        playerCharacter.resetStepsForNextBattle();
        menu = new Menu(playerCharacter);
        overworldState = new Overworld(viewSize, playerCharacter);
    }

    public GameMode getPreviousMode() {

        return previousGameMode;
    }

    public void setPreviousMode(GameMode previousGameMode) {

        this.previousGameMode = gameMode;
    }

    public GameMode getGameMode() {

        return gameMode;
    }

    public void setGameMode(GameMode gameMode) {

        this.gameMode = gameMode;
    }

    /**
     * Get the player's character.
     * 
     * @return Character playerCharacter
     */
    public Character getPlayerCharacter() {

        return playerCharacter;
    }

    /**
     * Handle the input and update the game accordingly.
     * 
     * @param int keyCode of key pressed by user.
     */
    public void update(InputCommand inputCommand) {


            //System.out.println("Game.update(): gameMode = " + gameMode);
            if (gameMode == GameMode.OVERWORLD) {
                if (playerCharacter.getStepsForNextBattle() == 0) {
                    startBattle();
                }
                if (inputCommand == InputCommand.ITEMS) {
                    setPreviousMode(gameMode);
                    gameMode = GameMode.MENU;
                    menu.setInProgress(true);
                }
                else {
                    overworldState.update(inputCommand);
                }
            }
            else if (gameMode == GameMode.BATTLE) {
                battle.tick(inputCommand);
                if (!battle.inProgress()) overworldState.setInProgress(true);
            }
            else if (gameMode == GameMode.MENU) {
                menu.tick(inputCommand);
                if (!menu.inProgress()) overworldState.setInProgress(true);
            }
            updateGameMode();
            previousInputCommand = inputCommand;
        
    }

    private void startBattle() {

        enemy =
            new Character("enemy", playerCharacter.getLocation(), new Color(
                0,
                127,
                255), playerCharacter
                .getLocation()
                .getGameMap()
                .getEnemyLevel(), "/resources/enemy.png");
        battle = new Battle(playerCharacter, enemy);
        setPreviousMode(gameMode);
        gameMode = GameMode.BATTLE;
    }

    private void updateGameMode() {

        if (playerCharacter.isAlive()) {
            previousGameMode = gameMode;
            if (menu.inProgress()) gameMode = GameMode.MENU;
            else if (battle.inProgress()) gameMode = GameMode.BATTLE;
            else if (overworldState.inProgress()) gameMode = GameMode.OVERWORLD;
        }
        else {
            // Player Character is dead
            gameMode = GameMode.GAMEOVER;
            System.out.println("GAME OVER");
        }
    }

    public boolean modeChanged() {

        return getPreviousMode() != getGameMode();
    }

    public Character getEnemyCharacter() {

        return enemy;
    }

    public Menu getMenu() {

        return menu;
    }

    public Battle getBattle() {

        return battle;
    }

    public int getViewSize() {

        return viewSize;
    }

    public void setViewSize(int viewSize) {

        this.viewSize = viewSize;
    }
}
