package mainApp;

import input.Controller;
import userInterface.GUI;

public class MainApp {

    public static void main(String[] args) {

        // init
        Game game = new Game();
        Controller controller = new Controller();
        GUI gui = new GUI(game, controller);
        InputCommand inputCommand = InputCommand.NOTHING;
        long startTime = System.currentTimeMillis();
        final long MILLISECONDS_PER_FRAME = 2000;

        // Game Loop
        while (true) {
            System.out.println("start of main game loop");
            startTime = System.currentTimeMillis();
            inputCommand = controller.getInputCommand();
            System.out.println("MainApp.main(): inputCommand: " + inputCommand);
            game.update(inputCommand);
            gui.render();
            try {
                Thread.sleep(startTime
                    + MILLISECONDS_PER_FRAME
                    - System.currentTimeMillis());
            }
            catch (InterruptedException e) {
                System.exit(1);
                e.printStackTrace();
            }
            System.out.println("end of main game loop");
            System.out.println();
        }
    }
}
