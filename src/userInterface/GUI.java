package userInterface;

import input.Controller;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JFrame;

import mainApp.Game;
import mainApp.GameMode;
import world.Character;

/**
 * Class that is used to instantiate the main GUI.
 * 
 * @author fin
 * 
 */
public class GUI extends JFrame {

	private static final long serialVersionUID = -1398591640190900149L;
	private MapView mapView;
	private int viewSize = 9;
	private int tileSize = 64;
	private Game game;
	private GameMode viewMode;
	private Character playerCharacter;
	public BattlePanel battlePanel;
	public MenuPanel menuPanel;
	public MapPanel mapPanel;
	public GameOverPanel gameOverPanel;
    public Controller controller;

	/**
	 * Constructs a new GUI for a given Game.
	 * 
	 * @param game a Game object that the GUI provides I/O for.
	 */
	public GUI(Game game, Controller controller) {
		this.game = game;
		game.setViewSize(viewSize);
		setTitle("RPGTestGame");
		playerCharacter = game.getPlayerCharacter();
		mapView = new MapView(playerCharacter, viewSize);
		viewMode = game.getGameMode();

		// Map Panel
		mapPanel = new MapPanel(mapView, tileSize);
		mapPanel.setPreferredSize(new Dimension(tileSize * viewSize, tileSize * viewSize));
		this.add(mapPanel, BorderLayout.CENTER);
		mapPanel.setBackground(new Color(0, 0, 0));
		
		// Initialise gui
		this.controller = controller;
		this.addKeyListener(controller);
		this.setFocusable(true);
		this.requestFocus(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.pack();
		this.setResizable(false);
		this.setLocationRelativeTo(null); // centres window in screen
		this.setVisible(true);
		this.requestFocusInWindow();
	}

	private void initBattlePanel(Character enemyCharacter) {
		battlePanel = new BattlePanel(game.getBattle(), playerCharacter, enemyCharacter, viewSize, tileSize);
		battlePanel.setPreferredSize(new Dimension(tileSize * viewSize, tileSize * viewSize));
		battlePanel.setBackground(new Color(0, 0, 0));
	}

	private void initMenuPanel() {
		menuPanel = new MenuPanel(game.getMenu(), tileSize);
		menuPanel.setPreferredSize(new Dimension(tileSize * viewSize, tileSize * viewSize));
		menuPanel.setBackground(new Color(0, 0, 0));
	}

	private void initGameOverPanel() {
		gameOverPanel = new GameOverPanel(playerCharacter, tileSize, viewSize);
		gameOverPanel.setPreferredSize(new Dimension(tileSize * viewSize, tileSize * viewSize));
		gameOverPanel.setBackground(new Color(0, 0, 0));
	}

	public void render() {
		if (game.getGameMode() != viewMode)
			changeMode(game.getGameMode());
		draw();
	}

	private void draw() {
		// System.out.println("GUI.draw() viewMode: " + viewMode);
		switch (viewMode) {
		case OVERWORLD:
			drawMap();
			break;
		case BATTLE:
			drawBattle();
			break;
		case GAMEOVER:
			drawGameOver();
			break;
		case MENU:
			drawMenu();
			break;
		default:
			break;
		}
	}

	private void drawBattle() {
		battlePanel.repaint();
	}

	private void drawMenu() {
		menuPanel.repaint();
	}

	private void drawGameOver() {
		gameOverPanel.repaint();
	}

	private void drawMap() {
		mapView.updateView();
		mapPanel.repaint();
	}

	private void removeMode(GameMode mode) {
		switch (mode) {
		case BATTLE:
			this.remove(battlePanel);
			break;
		case GAMEOVER:
			this.remove(gameOverPanel);
			break;
		case MENU:
			this.remove(menuPanel);
			break;
		case OVERWORLD:
			this.remove(mapPanel);
			break;
		default:
			break;
		}
	}

	private void changeMode(GameMode gameMode) {
		GameMode previousMode = viewMode;
		removeMode(previousMode);
		viewMode = gameMode;
		switch (viewMode) {
		case BATTLE:
			initBattlePanel(game.getEnemyCharacter());
			this.add(battlePanel, BorderLayout.CENTER);
			this.pack();
			break;
		case GAMEOVER:
			initGameOverPanel();
			this.add(gameOverPanel, BorderLayout.CENTER);
			this.pack();
			break;
		case MENU:
			initMenuPanel();
			this.add(menuPanel, BorderLayout.CENTER);
			this.pack();
			break;
		case OVERWORLD:
			this.add(mapPanel, BorderLayout.CENTER);
			this.pack();
			break;
		default:
			break;
		}
	}
}
