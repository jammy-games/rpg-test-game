package userInterface;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JPanel;

import world.Character;

/**
 * An extension of JPanel to provide a view of the player character's current
 * location on a map to the player.
 * 
 * @author fin
 * 
 */
public class GameOverPanel extends JPanel {

    /**
     * 
     */
    private static final long serialVersionUID = -1476052127905563616L;
    private int tileSize;
    private int viewSize;
    private Character playerCharacter;

    /**
     * Constructs a new map panel with a set sized view of a map.
     * 
     * @param mapView
     *        a View object. Represents the area of the map to be displayed in
     *        this panel.
     * 
     * @param tileSize
     *        an integer that dictates the size of each map tile in pixels.
     */
    public GameOverPanel (Character playerCharacter, int tileSize, int viewSize) {
        this.tileSize = tileSize;
        this.viewSize = viewSize;
        this.playerCharacter = playerCharacter;
    }

    /**
     * The draw method inherited from a JPanel. Called magically by java swing
     * somehow... It defines what is to be drawn in this panel.
     * 
     * @param g
     *        Graphics.
     */
    public void draw (Graphics g) {
        g.setColor(new Color(255, 255, 255));
        g.setFont(new Font("Dialog", Font.PLAIN, 20));
        g.drawString("GAME OVER", (tileSize * viewSize) / 2,
                (tileSize * viewSize) / 2);
        g.drawString("Total kills: " + playerCharacter.getKills(),
                (tileSize * viewSize) / 2, (tileSize * viewSize) / 2 + tileSize);

    }

    /**
     * Calls the UI delegate's paint method, if the UI delegate is non-null. We
     * pass the delegate a copy of the Graphics object to protect the rest of
     * the paint code from irrevocable changes (for example,
     * Graphics.translate). If you override this in a subclass you should not
     * make permanent changes to the passed in Graphics. For example, you should
     * not alter the clip Rectangle or modify the transform. If you need to do
     * these operations you may find it easier to create a new Graphics from the
     * passed in Graphics and manipulate it. Further, if you do not invoker
     * super's implementation you must honour the opaque property, that is if
     * this component is opaque, you must completely fill in the background in a
     * non-opaque colour. If you do not honour the opaque property you will
     * likely see visual artifacts. The passed in Graphics object might have a
     * transform other than the identify transform installed on it. In this
     * case, you might get unexpected results if you cumulatively apply another
     * transform.
     */
    @Override
    public void paintComponent (Graphics g) {
        super.paintComponent(g);
        draw(g);
    }
}
