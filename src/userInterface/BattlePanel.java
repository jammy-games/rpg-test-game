package userInterface;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JPanel;

import world.Battle;
import world.Character;

/**
 * 
 * 
 * @author fin
 * 
 */
public class BattlePanel extends JPanel {

    /**
     * 
     */
    private static final long serialVersionUID = -3316379782443132162L;
    private int viewSize;
    private int tileSize;
    private Character playerCharacter;
    private Character enemyCharacter;
    private Battle battle;

    public BattlePanel (
        Battle battle,
        Character playerCharacter,
        Character enemyCharacter,
        int viewSize,
        int tileSize) {

        this.playerCharacter = playerCharacter;
        this.enemyCharacter = enemyCharacter;
        this.battle = battle;
        this.viewSize = viewSize;
        this.tileSize = tileSize;
    }

    /**
     * The draw method inherited from a JPanel. Called magically by java swing
     * somehow... It defines what is to be drawn in this panel.
     * 
     * @param g
     *        Graphics.
     */
    public void draw(Graphics g) {

        // Draw background
        // switch (playerCharacter.getLocation().getTerrain()) {
        // case FOREST:
        // g.setColor(new java.awt.Color(0, 127, 0));
        // break;
        // case GRASS:
        // g.setColor(new java.awt.Color(0, 191, 0));
        // break;
        // case MOUNTAIN:
        // g.setColor(new java.awt.Color(127, 127, 127));
        // break;
        // case WATER:
        // g.setColor(new java.awt.Color(0, 127, 255));
        // break;
        // default:
        g.setColor(new java.awt.Color(0, 0, 0));
        // break;
        // }
        g.fillRect(0, 0, viewSize * tileSize, viewSize * tileSize);
        // Draw sky
        // g.setColor(new java.awt.Color(127, 159, 255));
        g.fillRect(0, 0, (viewSize * tileSize), (viewSize * tileSize) / 4);
        // Draw text background
        g.setColor(new java.awt.Color(0, 0, 0));
        int textBackgroundHeight = (viewSize * tileSize) / 3;
        g.fillRect(
            0,
            textBackgroundHeight + (viewSize * tileSize / 3),
            viewSize * tileSize,
            textBackgroundHeight);
        g.fillRect(0, 0, viewSize * tileSize, tileSize + (tileSize / 2));
        // Draw enemy
        int enemyBattleX = (viewSize - 3) * tileSize;
        drawCharacter(enemyCharacter, enemyBattleX, (2 * tileSize), g);
        drawCharacterHealth(enemyCharacter, 5 * tileSize, (tileSize), g);
        // Draw player
        int playerBattleX = tileSize;
        drawCharacter(playerCharacter, playerBattleX, (2 * tileSize), g);
        drawCharacterHealth(playerCharacter, playerBattleX, (tileSize), g);
        drawBattleMessage(g);
    }

    private void drawBattleMessage(Graphics g) {

        g.setFont(new Font("Dialog", Font.PLAIN, tileSize / 3));
        g.setColor(new Color(255, 255, 255));
        g.drawString(battle.getPreviousMessage(), tileSize, (tileSize * 7));
        g.drawString(battle.getMessage(), tileSize, (tileSize * 8));
    }

    private void drawCharacter(Character character, int x, int y, Graphics g) {

        g.setColor(character.getColor());
        g.fillRect(x, y, tileSize * 2, tileSize * 2);
        System.out.println("BattlePanel.drawCharacter()");
        g.drawImage(character.getSprite(), x, y, x+2*tileSize, y+2*tileSize, 0, 0, 16, 16, null);
    }

    private void drawCharacterHealth(
        Character character,
        int x,
        int y,
        Graphics g) {

        g.setFont(new Font("Dialog", Font.PLAIN, tileSize / 3));
        g.setColor(character.getColor());
        g.drawString(character.getName()
            + "'s HP: "
            + character.getHealth()
            + "/"
            + character.getMaxHealth(), x, y);
    }

    /**
     * Calls the UI delegate's paint method, if the UI delegate is non-null. We
     * pass the delegate a copy of the Graphics object to protect the rest of
     * the paint code from irrevocable changes (for example,
     * Graphics.translate). If you override this in a subclass you should not
     * make permanent changes to the passed in Graphics. For example, you should
     * not alter the clip Rectangle or modify the transform. If you need to do
     * these operations you may find it easier to create a new Graphics from the
     * passed in Graphics and manipulate it. Further, if you do not invoker
     * super's implementation you must honour the opaque property, that is if
     * this component is opaque, you must completely fill in the background in a
     * non-opaque colour. If you do not honour the opaque property you will
     * likely see visual artifacts. The passed in Graphics object might have a
     * transform other than the identify transform installed on it. In this
     * case, you might get unexpected results if you cumulatively apply another
     * transform.
     */
    @Override
    public void paintComponent(Graphics g) {

        super.paintComponent(g);
        draw(g);
    }
}
