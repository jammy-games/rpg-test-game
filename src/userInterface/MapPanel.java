package userInterface;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import world.Tile;

/**
 * An extension of JPanel to provide a view of the player character's current
 * location on a map to the player.
 * 
 * @author fin
 * 
 */
public class MapPanel extends JPanel {

    /**
     * 
     */
    private static final long serialVersionUID = -1476052127905563616L;
    private MapView mapView;
    private int VIEW_SIZE;
    private Tile[][] currentViewTiles;
    private int tileSize;

    /**
     * Constructs a new map panel with a set sized view of a map.
     * 
     * @param mapView
     *        a View object. Represents the area of the map to be displayed in
     *        this panel.
     * 
     * @param tileSize
     *        an integer that dictates the size of each map tile in pixels.
     */
    public MapPanel (MapView mapView, int tileSize) {

        this.mapView = mapView;
        this.tileSize = tileSize;
        this.VIEW_SIZE = mapView.getViewSize();
        currentViewTiles = mapView.getView();
    }

    /**
     * The draw method inherited from a JPanel. Called magically by java swing
     * somehow... It defines what is to be drawn in this panel.
     * 
     * @param g
     *        Graphics.
     * @throws IOException
     */
    public void draw(Graphics g) {

        // Get what should be visible.
        currentViewTiles = mapView.getView();
        String filePath = "/resources/empty.png";
        // Loop through tiles and draw correct terrain.
        BufferedImage image;
        for (int x = 0; x < VIEW_SIZE; ++x) {
            for (int y = 0; y < VIEW_SIZE; ++y) {
                switch (currentViewTiles[x][y].getTerrain()) {
                    case FOREST:
                        g.setColor(new java.awt.Color(0, 127, 0));
                        filePath = "/resources/forest.png";
                        break;
                    case GRASS:
                        g.setColor(new java.awt.Color(0, 191, 0));
                        filePath = "/resources/grass.png";
                        break;
                    case MOUNTAIN:
                        g.setColor(new java.awt.Color(127, 127, 127));
                        filePath = "/resources/mountain.png";
                        break;
                    case WATER:
                        g.setColor(new java.awt.Color(0, 127, 255));
                        filePath = "/resources/water.png";
                        break;
                    case DOOR:
                        g.setColor(new java.awt.Color(0, 127, 255));
                        filePath = "/resources/door.png";
                        break;
                    case EMPTY:
                        g.setColor(new java.awt.Color(0, 0, 0));
                        filePath = "/resources/empty.png";
                        break;
                    default:
                        g.setColor(new java.awt.Color(0, 0, 0));
                        filePath = "/resources/texture.png";
                        break;
                }
                // TODO Placeholder for texture. Just a coloured square for now.
                g.fillRect(
                    x * tileSize,
                    y * tileSize,
                    tileSize - 1,
                    tileSize - 1);
                try {
                    image = ImageIO.read(getClass().getResource(filePath));
                    g.drawImage(
                        image,
                        x * tileSize,
                        y * tileSize,
                        (x * tileSize) + tileSize,
                        (y * tileSize) + tileSize,
                        0,
                        0,
                        16,
                        16,
                        null);
                }
                catch (IOException e) {
                    e.printStackTrace();
                    System.err.println("Error trying to read file " + filePath);
                }
            }
        }
        
        
        // draws characters after terrain is drawn
        for (int x = 0; x < VIEW_SIZE; ++x) {
            for (int y = 0; y < VIEW_SIZE; ++y) {
                if (currentViewTiles[x][y].isOccupied()) {
                    g.setColor(currentViewTiles[x][y].getOccupier().getColor());
                    g.fillRect(
                        ((VIEW_SIZE - 1) / 2) * tileSize,
                        ((VIEW_SIZE - 1) / 2) * tileSize,
                        tileSize - 1,
                        tileSize - 1);
                    g.drawImage(currentViewTiles[x][y].getOccupier().getSprite(), ((VIEW_SIZE - 1) / 2) * tileSize,
                        ((VIEW_SIZE - 1) / 2) * tileSize, tileSize + ((VIEW_SIZE - 1) / 2) * tileSize,
                        tileSize + ((VIEW_SIZE - 1) / 2) * tileSize, 0, 0, 16, 16, null);
                }
            }
        }
    }

    /**
     * Calls the UI delegate's paint method, if the UI delegate is non-null. We
     * pass the delegate a copy of the Graphics object to protect the rest of
     * the paint code from irrevocable changes (for example,
     * Graphics.translate). If you override this in a subclass you should not
     * make permanent changes to the passed in Graphics. For example, you should
     * not alter the clip Rectangle or modify the transform. If you need to do
     * these operations you may find it easier to create a new Graphics from the
     * passed in Graphics and manipulate it. Further, if you do not invoker
     * super's implementation you must honour the opaque property, that is if
     * this component is opaque, you must completely fill in the background in a
     * non-opaque colour. If you do not honour the opaque property you will
     * likely see visual artifacts. The passed in Graphics object might have a
     * transform other than the identify transform installed on it. In this
     * case, you might get unexpected results if you cumulatively apply another
     * transform.
     */
    @Override
    public void paintComponent(Graphics g) {

        super.paintComponent(g);
        draw(g);
    }
}
