/**
 * 
 */
package userInterface;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.LayoutManager;
import java.util.ArrayList;

import javax.swing.JPanel;

import world.Item;
import world.Menu;

/**
 * @author fin
 * 
 */
public class MenuPanel extends JPanel {

    /**
     * 
     */
    private static final long serialVersionUID = 5281686523102388193L;
    private Menu menu;
    private ArrayList<Item> items;
    private int tileSize;

    /**
     * 
     */
    public MenuPanel(Menu menu, int tileSize) {
	this.menu = menu;
	this.items = menu.getItems();
	this.tileSize = tileSize;
    }

    public void draw(Graphics g) {
	g.setColor(new Color(255, 255, 255));
	g.setFont(new Font("Dialog", Font.PLAIN, 20));
	for (int i = 0; i < items.size(); i++) {
	    g.drawString(items.get(i).getName(), tileSize * 2,
		    ((tileSize * 2) + (i * (tileSize / 2))));
	    // Draw selection indicator

	    if (menu.getSlectedItem() == items.get(i)) {
		g.fillRect(tileSize,
			(((i * (tileSize / 2)) + tileSize) + (int)(tileSize * 0.75)),
			tileSize / 2, tileSize / 4);
	    }
	}
	g.drawString(menu.getMessage(), tileSize, tileSize);
    }

    @Override
    public void paintComponent(Graphics g) {
	super.paintComponent(g);
	draw(g);
    }

    /**
     * @param arg0
     */
    public MenuPanel(LayoutManager arg0) {
	super(arg0);
	// TODO Auto-generated constructor stub
    }

    /**
     * @param arg0
     */
    public MenuPanel(boolean arg0) {
	super(arg0);
	// TODO Auto-generated constructor stub
    }

    /**
     * @param arg0
     * @param arg1
     */
    public MenuPanel(LayoutManager arg0, boolean arg1) {
	super(arg0, arg1);
	// TODO Auto-generated constructor stub
    }

}
