package userInterface;

import world.Character;
import world.GameMap;
import world.Tile;

/**
 * A view of a map. To be rendered by the MapPanel.
 * 
 * @author fin
 * 
 */
public class MapView {

    private int viewSize;
    private Tile[][] view;
    private GameMap currentMap;
    private Character playerCharacter;

    /**
     * Constructs a new View of the player character's current location.
     * 
     * @param pc
     *            the player's Character
     * @param viewSize
     *            number of cells of the length of the square of tiles visible.
     */
    public MapView(Character pc, int viewSize) {
	this.playerCharacter = pc;
	this.viewSize = viewSize;
	view = new Tile[viewSize][viewSize];
	currentMap = playerCharacter.getLocation().getGameMap();
	updateView();
    }

    /**
     * returns the array of tiles in the current view.
     * 
     * @return Tile[][] view.
     */
    public Tile[][] getView() {
	return view;
    }

    /**
     * Update the view with the player character's location as centre.
     */
    public void updateView() {

	currentMap = playerCharacter.getLocation().getGameMap();
	// System.out.println("Current Map : " + currentMap.toString());
	int pcX = playerCharacter.getLocation().getX();
	int pcY = playerCharacter.getLocation().getY();

	int viewOffsetX = (viewSize - 1) / 2;
	int viewOffsetY = (viewSize - 1) / 2;

	int viewStartX = pcX - viewOffsetX;
	int viewStartY = pcY - viewOffsetY;

	for (int y = 0; y < viewSize; y++) {
	    for (int x = 0; x < viewSize; x++) {
		view[x][y] = currentMap.getTile(viewStartX + x, viewStartY + y);
	    }
	}
    }

    /**
     * returns the size of the view.
     * 
     * @return int viewSize.
     */
    public int getViewSize() {
	return viewSize;
    }

    /**
     * returns a particular tile in the view.
     * 
     * @param x
     *            coordinate of tile in current view
     * @param y
     *            coordinate of tile in current view
     * @return The Tile specified by x and y
     */
    public Tile getTile(int x, int y) {
	return view[x][y];
    }
}
