RPG Test Game
=============

A test to see if we can make a basic JRPG style game from scratch.

Builds (Play Testing)
---------------------

To try a build, download the latest `rpg-test-game_yyyy-mm-dd.jar` and run with
the command

    java -jar rpg-test-game_yyyy-mm-dd.jar

Controlls
---------

Currently all controlls are via the keyboard.

### Keys

*   A = attack
*   F = flee
*   I = item menu
*   X = exit item menu
*   Enter = use selected item
*   Arrow keys to move and select itmes in menu.